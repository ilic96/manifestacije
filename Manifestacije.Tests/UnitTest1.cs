﻿using Manifestacije.Controllers;
using Manifestacije.Interfaces;
using Manifestacije.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace Manifestacije.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetReturnsGradWithSameId()
        {
            // Arrange
            var mockRepository = new Mock<IMestoRepository>();
            mockRepository.Setup(x => x.GetById(42)).Returns(new Models.Mesto { Id = 42 });

            var controller = new MestaController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.GetMesto(42);
            var contentResult = actionResult as OkNegotiatedContentResult<Mesto>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(42, contentResult.Content.Id);
        }
        [TestMethod]
        public void GetReturnsNotFound()
        {
            // Arrange
            var mockRepository = new Mock<IMestoRepository>();
            var controller = new MestaController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.GetMesto(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }
        [TestMethod]
        public void GetReturnsMultipleObjects()
        {
            // Arrange
            List<Mesto> mesta = new List<Mesto>();
            mesta.Add(new Mesto { Id = 1, Naziv = "London", Postanski_kod = 2222 });
            mesta.Add(new Mesto { Id = 2, Naziv = "Bec", Postanski_kod = 1111 });

            var mockRepository = new Mock<IMestoRepository>();
            mockRepository.Setup(x => x.GetAll()).Returns(mesta.AsEnumerable());
            var controller = new MestaController(mockRepository.Object);

            // Act
            IEnumerable<Mesto> result = controller.GetMesta();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(mesta.Count, result.ToList().Count);
            Assert.AreEqual(mesta.ElementAt(0), result.ElementAt(0));
            Assert.AreEqual(mesta.ElementAt(1), result.ElementAt(1));
        }
    }
}
