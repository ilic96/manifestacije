﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Manifestacije.Models
{
    public class Festival
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Naziv { get; set; }
        [Required]
        public int Cena_karte { get; set; }
        [Required]
        [Range(1950, 2018)]
        public int Godina_odrzavanja { get; set; }
        [ForeignKey("Mesto")]
        public int MestoId { get; set; }
        public Mesto Mesto { get; set; }

    }
}