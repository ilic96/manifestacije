﻿using Manifestacije.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manifestacije.Interfaces
{
    public interface IFestivalRepository
    {
        IEnumerable<Festival> GetAll();
        IEnumerable<Festival> Filter(int start, int kraj);
        Festival GetById(int id);
        void Add(Festival festival);
        void Update(Festival festival);
        void Delete(Festival festival);
    }
}
