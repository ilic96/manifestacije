﻿using Manifestacije.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manifestacije.Interfaces
{
    public interface IMestoRepository
    {
        IEnumerable<Mesto> GetAll();
        IEnumerable<Mesto> Filter(int x);
        Mesto GetById(int id);
        void Add(Mesto mesto);
        void Update(Mesto mesto);
        void Delete(Mesto mesto);

    }
}
