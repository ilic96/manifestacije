﻿using Manifestacije.Interfaces;
using Manifestacije.Models;
using Manifestacije.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Manifestacije.Controllers
{
    public class MestaController : ApiController
    {
        private IMestoRepository _repository { get; set; }

        public MestaController(IMestoRepository repository)
        {
            _repository = repository;
        }
        //GET api/mesta
        [ResponseType(typeof(Mesto))]
        [Route("api/mesta")]
        public IEnumerable<Mesto> GetMesta()
        {
            return _repository.GetAll();
        }
        //GET api/mesta/{id}
        [ResponseType(typeof(Mesto))]
        [Route("api/mesta/{id}")]
        public IHttpActionResult GetMesto(int id)
        {
            var mesto = _repository.GetById(id);
            if(mesto == null)
            {
                return NotFound();
            }
            return Ok(mesto);
        }
        //GET api/mesta?kod={vrednost}
        [ResponseType(typeof(Mesto))]
        [Route("api/mesta/kod={x}")]
        public IEnumerable<Mesto> GetMesta(int x)
        {
            return _repository.Filter(x);
        }
    }
}
