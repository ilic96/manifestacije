﻿using Manifestacije.Interfaces;
using Manifestacije.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Manifestacije.Controllers
{
    public class FestivaliController : ApiController
    {
        private IFestivalRepository _repository { get; set; }

        public FestivaliController(IFestivalRepository repository)
        {
            _repository = repository;
        }
        //GET api/festivali
        [ResponseType(typeof(Festival))]
        [Route("api/festivali")]
        public IEnumerable<Festival> GetFestivali()
        {
            return _repository.GetAll();
        }
        //GET api/festivali/1
        [ResponseType(typeof(Festival))]
        [Route("api/festivali/{id}")]
        public IHttpActionResult GetFestival(int id)
        {
            var festival = _repository.GetById(id);
            if (festival == null)
            {
                return BadRequest();
            }
            return Ok(festival);
        }
        //POST api/festivali
        [Authorize]
        [ResponseType(typeof(Festival))]
        [Route("api/festivali")]
        public IHttpActionResult PostFestival(Festival festival)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _repository.Add(festival);
            return Ok(festival);
        }
        //PUT api/festivali/1
        [Authorize]
        [ResponseType(typeof(Festival))]
        [Route("api/festivali/{id}")]
        public IHttpActionResult PutFestival(int id, Festival festival)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != festival.Id)
            {
                return BadRequest();
            }
            try
            {
                _repository.Update(festival);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(festival);

        }
        //DELETE api/festivali/1
        [Authorize]
        [ResponseType(typeof(Festival))]
        [Route("api/festivali/{id}")]
        public IHttpActionResult DeleteFestival(int id)
        {
            var festival = _repository.GetById(id);
            if(festival == null)
            {
                return BadRequest();
            }
            _repository.Delete(festival);
            return Ok();
        }
        //POST   api/festivali/pretraga
        [ResponseType(typeof(Festival))]
        [Route("api/festivali/pretraga")]
        public IEnumerable<Festival> PostFestivalPretraga(int start, int kraj)
        {
            return _repository.Filter(start, kraj);
        }
    }
}
