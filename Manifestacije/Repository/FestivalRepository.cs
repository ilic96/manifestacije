﻿using Manifestacije.Interfaces;
using Manifestacije.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace Manifestacije.Repository
{
    public class FestivalRepository : IDisposable, IFestivalRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public void Add(Festival festival)
        {
            db.sviFestivali.Add(festival);
            db.SaveChanges();
        }

        public void Delete(Festival festival)
        {
            db.sviFestivali.Remove(festival);
            db.SaveChanges();
        }

        public IEnumerable<Festival> GetAll()
        {
            return db.sviFestivali.OrderByDescending(k => k.Cena_karte);
        }

        public Festival GetById(int id)
        {
            return db.sviFestivali.FirstOrDefault(f => f.Id == id);
            
        }

        public void Update(Festival festival)
        {
            db.Entry(festival).State = System.Data.Entity.EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public IEnumerable<Festival> Filter(int start, int kraj)
        {
            IEnumerable<Festival> rezultat = from f in db.sviFestivali.Include(m => m.Mesto).OrderBy(p => p.Godina_odrzavanja) where f.Godina_odrzavanja < kraj && f.Godina_odrzavanja > start select f;
            return rezultat;
        }
    }
}