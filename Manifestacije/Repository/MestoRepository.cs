﻿using Manifestacije.Interfaces;
using Manifestacije.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace Manifestacije.Repository
{
    public class MestoRepository : IDisposable, IMestoRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public void Add(Mesto mesto)
        {
            db.svaMesta.Add(mesto);
            db.SaveChanges();
        }

        public void Delete(Mesto mesto)
        {
            db.svaMesta.Remove(mesto);
            db.SaveChanges();
        }

        public IEnumerable<Mesto> GetAll()
        {
            return db.svaMesta;
        }

        public Mesto GetById(int id)
        {
            return db.svaMesta.FirstOrDefault(m => m.Id == id);
        }

        public void Update(Mesto mesto)
        {
            db.Entry(mesto).State = System.Data.Entity.EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<Mesto> Filter(int x)
        {
            IEnumerable<Mesto> rezultat = from m in db.svaMesta.OrderBy(p => p.Postanski_kod) where m.Postanski_kod < x select m;
            return rezultat;
        }
    }
}