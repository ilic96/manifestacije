namespace Manifestacije.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Manifestacije.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Manifestacije.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            context.svaMesta.AddOrUpdate
                (
                    new Models.Mesto() { Id = 1, Naziv = "Beograd", Postanski_kod = 11000},
                    new Models.Mesto() { Id = 2, Naziv = "Novi Sad", Postanski_kod = 21000},
                    new Models.Mesto() { Id = 3, Naziv = "Sremska Mitrovica", Postanski_kod = 22000}
                );
            context.SaveChanges();
            context.sviFestivali.AddOrUpdate
                (
                    new Models.Festival() { Id = 1, Naziv = "Dortjol festival", Cena_karte = 650, Godina_odrzavanja = 2000, MestoId = 1},
                    new Models.Festival() { Id = 2, Naziv = "Exit", Cena_karte = 2500, Godina_odrzavanja = 2012, MestoId = 2},
                    new Models.Festival() { Id = 3, Naziv = "Beer fest", Godina_odrzavanja = 2015, Cena_karte = 1200, MestoId = 3}
                );
        }
    }
}
