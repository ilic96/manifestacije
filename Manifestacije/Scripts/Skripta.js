﻿$(window).on(function () {
    $("#festivali").click();
});

$(document).ready(function () {
    // podaci od interesa
    var host = window.location.host;
    var token = null;
    var headers = {};
    var formAction = "Create"; // dodavanje festivala
    var editingId; // id za PUT

    // posto inicijalno nismo prijavljeni, sakrivamo odjavu
    $("#odjava").css("display", "none");
    $("#bReg").click(function () {
        $("#registracijaP").css("display", "block");
        $("#bReg").css("display", "none");
    });
    // registracija korisnika
    $("#registracija").submit(function (e) {
        e.preventDefault();

        var email = $("#regEmail").val();
        var loz1 = $("#regLoz").val();
        var loz2 = $("#regLoz2").val();

        // objekat koji se salje
        var sendData = {
            "Email": email,
            "Password": loz1,
            "ConfirmPassword": loz2
        };


        $.ajax({
            type: "POST",
            url: 'http://' + host + "/api/Account/Register",
            data: sendData

        }).done(function (data) {
            clearForm();
            $("#info").append("Uspešna registracija. Možete se prijaviti na sistem.");

        }).fail(function (data) {
            alert(data);
        });
    });

    // prijava korisnika
    $("#prijava").submit(function (e) {
        e.preventDefault();

        var email = $("#priEmail").val();
        var loz = $("#priLoz").val();

        // objekat koji se salje
        var sendData = {
            "grant_type": "password",
            "username": email,
            "password": loz
        };

        $.ajax({
            "type": "POST",
            "url": 'http://' + host + "/Token",
            "data": sendData

        }).done(function (data) {
            console.log(data);
            $("#info").empty().append("Prijavljen korisnik: " + data.userName);
            token = data.access_token;
            $("#prijava").css("display", "none");
            $("#registracija").css("display", "none");
            $("#odjava").css("display", "block");

            $("#festivali").trigger("click");
         
        }).fail(function (data) {
            alert(data);
        });
    });

    // odjava korisnika sa sistema
    $("#odjavise").click(function () {
        token = null;
        headers = {};

        $("#info").empty();
        $("#data").empty();
        $("#festivali").trigger("click");
        $("#prijava").css("display", "block");
        $("#registracija").css("display", "block");
        $("#odjava").css("display", "none");
        $("#formDiv").css("display", "none");
        $("#priEmail").val('');
        $("#priLoz").val('');
    
    })

    // pripremanje fesivala za brisanje
    $("body").on("click", "#btnDelete", deleteFestival);

    // priprema festivala za izmenu
    $("body").on("click", "#btnEdit", editFestival);

    // ucitavanje festivala
    $("#festivali").click(function () {

        // korisnik mora biti ulogovan
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        $.ajax({
            "type": "GET",
            "url": "http://" + host + "/api/festivali",
            "headers": headers

        }).done(function (data, status) {
            clearForm();
            var $container = $("#data");
            $container.empty();

            if (status === "success") {
                console.log(data);
                // ispis festivala
                if (token) {
                    var div = $("<div></div>");
                    var h1 = $("<h1>Festivali</h1>");
                    div.append(h1);

                    // ispis tabele
                    var table = $("<table class='table table-hover table-bordered'></table>");
                    var header = $("<tr><th>Id</th><th>Naziv</th><th>Cena</th><th>Godina Odrzavanja</th><th>Mesto</th><th>Delete</th><th>Edit</th></tr>");
                   
                    table.append(header);

                    for (i = 0; i < data.length; i++) {
                        // prikazujemo novi red u tabeli
                        var row = "<tr>";
                        // prikaz podataka
                        var displayData = "<td>" + data[i].Id + "</td><td>" + data[i].Naziv + "</td><td>" + data[i].Cena_karte + "</td><td>" + data[i].Godina_odrzavanja + "</td><td>" + data[i].Mesto.Naziv + "</td>";
                        // prikaz dugmadi za izmenu i brisanje
                        var stringId = data[i].Id.toString();
                        var displayDelete = "<td><button class='btn-danger' id=btnDelete name=" + stringId + ">Delete</button></td>";
                        var displayEdit = "<td><button class='btn-primary' id=btnEdit name=" + stringId + ">Edit</button></td>";
                        row += displayData + displayDelete + displayEdit + "</tr>";
                        table.append(row);
                        newId = data[i].Id;

                        console.log(data);
                    }

                    div.append(table);

                    // prikaz forme
                    $("#formDiv").css("display", "block");

                    // ispis novog sadrzaja
                    $container.append(div);

                } else {
                    div = $("<div></div>");
                    h1 = $("<h1>Festivali</h1>");
                    div.append(h1);

                    // ispis tabele
                    table = $("<table class='table table-hover table-bordered'></table>");
                    header = $("<tr><th>Id</th><th>Naziv</th><th>Cena</th><th>Godina Odrzavanja</th><th>Mesto</th></tr>");
                    table.append(header);

                    for (i = 0; i < data.length; i++) {
                        // prikazujemo novi red u tabeli
                        row = "<tr>";
                        // prikaz podataka
                        displayData = "<td>" + data[i].Id + "</td><td>" + data[i].Naziv + "</td><td>" + data[i].Cena_karte + "</td><td>" + data[i].Godina_odrzavanja + "</td><td>" + data[i].Mesto.Naziv + "</td>";
                        stringId = data[i].Id.toString();

                        row += displayData + "</tr>";
                        table.append(row);
                        newId = data[i].Id;

                        console.log(data);
                    }

                    div.append(table);

                    // ispis novog sadrzaja
                    $container.append(div);
                }

            }
            else {
                div = $("<div></div>");
                h1 = $("<h1>Greška prilikom preuzimanja festivala!</h1>");
                div.append(h1);
                $container.append(div);
            }

        }).fail(function (data) {
            alert(data.status + ": " + data.statusText);
        });


    });

    // Brisanje festivala
    function deleteFestival() {

        // izvlacimo {id}
        var deleteId = this.name;
        // saljemo zahtev 
        $.ajax({
            "type": "DELETE",
            "url": "http://" + host + "/api/festivali/" + deleteId.toString(),
            "headers": headers
        }).done(function (data, status) {
            refreshTable();
        }).fail(function (data, status) {
            alert("Desila se greska!");
        });

    };

    // Izmena festivala
    function editFestival() {
        // izvlacimo id
        var editId = this.name;
        // saljemo zahtev da dobavimo taj festival
        $.ajax({
            "type": "GET",
            "url": "http://" + host + "/api/festivali/" + editId.toString(),
            "headers": headers
        }).done(function (data, status) {
            $("#festivalNaziv").val(data.Naziv);
            $("#festivalCena_karte").val(data.Cena_karte);
            $("#festivalGodina_odrzavanja").val(data.Godina_odrzavanja);
            $("#festivalMesto").val(data.MestoId);
            editingId = data.Id;
            formAction = "Update";
        }).fail(function (data, status) {
            formAction = "Create";
            alert("Desila se greska!");
        });

    };

    // add i edit festivala
    $("#festivalForm").submit(function (e) {
        // sprecavanje default akcije forme
        e.preventDefault();

        var festivalNaziv = $("#festivalNaziv").val();
        var festivalCena = $("#festivalCena_karte").val();
        var festivalGodina = $("#festivalGodina_odrzavanja").val();
        var festivalMesto = $("#festivalMesto").val();
        var httpAction;
        var sendData;
        var url;

        // u zavisnosti od akcije pripremamo objekat
        if (formAction === "Create") {
            httpAction = "POST";
            url = "http://" + host + "/api/festivali/";
            console.log("URL:" + url);
            sendData = {
                "Naziv": festivalNaziv,
                "Cena_karte": festivalCena,
                "Godina_odrzavanja": festivalGodina,
                "MestoId": festivalMesto

            };
        }
        else {
            httpAction = "PUT";
            url = "http://" + host + "/api/festivali/" + editingId.toString();
            sendData = {
                "Id": editingId,
                "Naziv": festivalNaziv,
                "Cena_karte": festivalCena,
                "Godina_odrzavanja": festivalGodina,
                "MestoId": festivalMesto
            };
        }

        console.log("Objekat za slanje");
        console.log(sendData);

        $.ajax({
            url: url,
            type: httpAction,
            "headers": headers,
            data: sendData
        })
            .done(function (data, status) {
                formAction = "Create";
                refreshTable();
            })
            .fail(function (data, status) {
                alert("Desila se greska!");
            })

    });

    // osvezavanje prikaz tabele
    function refreshTable() {
        // cistimo formu
        $("#festivalNaziv").val('');
        $("#festivalCena_karte").val('');
        $("#festivalGodina_odrzavanja").val('');
        $("#festivalMesto").val('');

        // osvezavamo
        $("#festivali").trigger("click");
    };

    // osvezavanje forme
    function clearForm() {
        // clear forme
        $("#festivalNaziv").val('');
        $("#festivalCena_karte").val('');
        $("#festivalGodina_odrzavanja").val('');
        $("#festivalMesto").val('');


        // clear registracija
        $("#regEmail").val('').empty();
        $("#regLoz").val('').empty();
        $("#regLoz2").val('').empty();
    };

});